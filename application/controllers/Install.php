<?php
/**
 * Created by PhpStorm.
 * User: RVR LP 3
 * Date: 1/29/2018
 * Time: 4:21 PM
 */

use Illuminate\Database\Capsule\Manager as Capsule;

class Install extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index() {

        Capsule::schema()->dropIfExists('diet');
        Capsule::schema()->create('diet', function ($table){
            $table->increments('id');
            $table->string('name', 255);
            $table->string('description', 255)->nullable();
            $table->tinyInteger('sort_order')->default(0);
            $table->timestamps();
        });


        Capsule::schema()->dropIfExists('');


        echo 'Tables created';

    }
}