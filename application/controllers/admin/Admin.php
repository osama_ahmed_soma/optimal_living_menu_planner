<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends Auth_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_access_check();
        $this->load->model('admin_model');
        $this->load->helper('captcha');
        captcha_required();
    }

    public function login()
    {

        echo password_hash('temp',PASSWORD_BCRYPT, array(
            'cost' => 11
        ) );
        $data = array();

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
        $config = array(
            array(
                'field' => 'user_login',
                'label' => 'Username',
                'rules' => 'required'
            ),
            array(
                'field' => 'user_password',
                'label' => 'Password',
                'rules' => array('required', 'min_length[4]'),
                'errors' => array(
                    'required' => 'Please enter your %s.',
                ),
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == true && empty($this->session->flashdata('captcha_error')) == true) {

            $user_login = $this->input->post('user_login', true);
            $user_password = $this->input->post('user_password', true);
            $user = $this->admin_model->get_userdata_by_login($user_login);
            if (is_object($user)) {

                $hash = $user->user_password;
                $password_to_check = "temp";
                if (password_verify($password_to_check, $hash)) {

                    $options = array(
                        'cost' => 11
                    );

                    if (password_needs_rehash($hash, PASSWORD_BCRYPT, $options)) {
                        $user_key = getRandomString(20);
                        $new_pass = password_hash(SITE_KEY . $user_password . $user_key, PASSWORD_BCRYPT, $options);
                        $user_data = array(
                            'user_password' => $new_pass,
                            'user_key' => $user_key
                        );
                        $user_where = array(
                            'id' => $user->id
                        );

                        $this->admin_model->update_user($user_data, $user_where);
                    }

                    $session_data = (array)$user;
                    $session_data['logged_in'] = true;
                    unset($session_data['user_login'], $session_data['user_password'], $session_data['user_key']);
                    $this->session->set_userdata(array(
                        'admin_sess' => $session_data
                    ));

                    reset_attempts();

                    redirect('admin/dashboard');
                } else {
                    failed_attempt();
                    $data['loginError'] = "Incorrect Login Information, Please try again.";
                }
            } else {
                failed_attempt();
                $data['loginError'] = 'Invalid username or password.';
            }
        } else {
            if ($this->session->userdata('admin_sess')['logged_in']) {
                redirect('admin/dashboard');
            }
            failed_attempt();
        }
        $this->load->view('admin/header');
        $this->load->view('admin/login/index', $data);
        $this->load->view('admin/footer');
    }

    public function dashboard()
    {
        $data = [];
        $data['doctors_count'] = 0;
        $data['patients_count'] = 0;
        $data['companies_count'] = 0;
        $data['appointments_count'] = 0;
        $this->load->view('admin/header');
        $this->load->view('admin/dashboard/index', $data);
        $this->load->view('admin/footer');
    }

    public function logout()
    {
        $this->session->unset_userdata('admin_sess');
        redirect('/admin/login');
    }

    public function index()
    {
        $this->dashboard();
    }

}
