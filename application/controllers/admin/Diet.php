<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Diet extends Auth_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_access_check();
        $this->load->model('Diet_model');
    }

    public function index($page = 0)
    {

        $data = array();

        $config = array(
            'limit' => 10,
            'count' => Diet_model::all()->count(),
            'url' => 'admin/diet/index/'
        );

        $offset = ($page == 0) ? 0 : ($page * $config['limit']) - $config['limit'];

        $data['diets'] = Diet_model::take($config['limit'])
            ->skip($offset)
            ->orderBy('id', 'desc')->get();

        $data['pagination_link'] = $this->pagination_setup($config);
        $this->load->view('admin/header');
        $this->load->view('admin/diet/index', $data);
        $this->load->view('admin/footer');
    }

    private function pagination_setup($setup_data = array())
    {
        $this->load->library('pagination');
        $limit = $setup_data['limit'];
        $config = array();
        $config['reuse_query_string'] = FALSE;
        $config['total_rows'] = $setup_data['count'];
        $config['use_page_numbers'] = TRUE;

        //bootstrap changes
        $config['full_tag_open'] = '<nav aria-label="Page navigation" style="text-align: center;"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class=" page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class=" page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class=" page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');

        //pagination value
        $config['base_url'] = base_url() . $setup_data['url'];
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    public function diet_ajax()
    {

        $data = array(
            'message' => '',
            'diet' => array(),
            'bool' => false
        );

        if (!$this->input->is_ajax_request()) {
            $data['message'] = 'No direct script access allowed';
            die(json_encode($data));
        }

        $form_data = array_map('trim',$this->input->post());


        if ( $this->input->post('remove') ) {
            $diet_id = trim($this->input->post('diet_id'));
            $diet = Diet_model::find($diet_id);
            $diet->delete();
            die('deleted');
        }

        if ( $form_data['name'] == '' ) {
            $data['message'] = 'Name is required.';
            die(json_encode($data));
        }

        $diet = Diet_model::where('name', $form_data['name'])->first();

        if ( isset( $diet->id )  && (int)$form_data['id'] == 0) {
            $data['message'] = 'This diet ' . $form_data['name'] . ' is already in record, please use another name to create new diet';
            die(json_encode($data));
        }

        $diet = ( (int)$form_data['id'] == 0 ) ? Diet_model::create($form_data) : Diet_model::where('id', $form_data['id'])->update($form_data);

        $data['diet'] = $diet;
        $data['message'] = ( ( int ) $form_data['id'] == 0 ) ? 'New Diet Added' : 'Diet updated';
        $data['bool'] = true;
        die(json_encode($data));
    }


    public function edit_diet_ajax()
    {

        $data = array(
            'message' => '',
            'diet' => array(),
            'bool' => false
        );

        if (!$this->input->is_ajax_request()) {
            $data['message'] = 'No direct script access allowed';
            die(json_encode($data));
        }


        if ( $this->input->post('remove') ) {
            $diet_id = trim($this->input->post('diet_id'));
            $diet = Diet_model::find($diet_id);
            $diet->delete();
            die();
        }

        $diet_name = trim($this->input->post('diet_name'));
        $diet_id = trim($this->input->post('diet_id'));

        if (empty($diet_name)) {
            $data['message'] = 'Diet name is required.';
            die(json_encode($data));
        }

        $diet = Diet_model::find($diet_id);

        if (!isset($diet->id)) {
            $data['message'] = 'Update failed. Diet not found.';
            die(json_encode($data));
        }

        $diet_existed = Diet_model::where('name', $diet_name)
            ->where('id', '!=', $diet_id)
            ->first();

        if (isset($diet_existed->id)) {
            $data['message'] = 'This diet ' . $diet_name . ' is already in record, please use another name to create new diet.';
            die(json_encode($data));
        }


        $diet = Diet_model::where('id', $diet_id)
            ->update(['name' => $diet_name]);

        $data['diet'] = $diet;
        $data['message'] = 'Diet Updated';
        $data['bool'] = true;
        die(json_encode($data));

    }


    public function remove_diet_ajax()
    {

        $data = array(
            'message' => '',
            'diet' => array(),
            'bool' => false
        );

        if (!$this->input->is_ajax_request()) {
            $data['message'] = 'No direct script access allowed';
            die(json_encode($data));
        }



        $data = array(
            'message' => '',
            'diet' => array(),
            'bool' => false
        );

    }

}