<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($limit = null, $page = null){
        $this->db->order_by('id', 'ASC');
        $this->db->from('admin_users');
        if ($limit !== null) {
            $this->db->limit($limit, $page);
        }
        return $this->db->get()->result();
    }

    public function getAll_count(){
        return count($this->getAll());
    }

    public function get_userdata_by_login($user_login) {
        return $this->db->select('*')
            ->from('admin_users')
            ->where('user_login', $user_login)
            ->where('user_status', '1')
            ->limit('1')
            ->get()
            ->row();
    }

    public function update_user( $data,  $where) {
        $this->db->update('admin_users', $data, $where);
        return $this->db->affected_rows();
    }


}