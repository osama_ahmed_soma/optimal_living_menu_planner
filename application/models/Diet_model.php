<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Diet_model extends Eloquent
{

    protected $table = 'diet';

    protected $fillable = array('name', 'description', 'sort_order');

    public function is_exists($fields = array())
    {
        if (count($fields) <= 0) {
            return false;
        }
        foreach ($fields as $field => $value) {
            $this->db->where($field, $value);
        }
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }


    public function getByID($dietID)
    {
        $this->db->where('id', $dietID);
        $this->db->from($this->table);
        return $this->db->get()->row();
    }

    /*
     * Function to insert record
     *
     * @since 1.0
     * @author Rizwan
     */
    public function add(Array $data) {

        return $this->db->insert($this->table, $data);

    }


}