<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Your Choice. Your Health. Your Way</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
    <link href="<?php echo base_url(); ?>template_file/img/favicon/apple-icon-144x144.png" rel="apple-touch-icon"
          type="image/png" sizes="144x144">
    <link href="<?php echo base_url(); ?>template_file/img/favicon/apple-icon-114x114.png" rel="apple-touch-icon"
          type="image/png" sizes="114x114">
    <link href="<?php echo base_url(); ?>template_file/img/favicon/apple-icon-72x72.png" rel="apple-touch-icon"
          type="image/png" sizes="72x72">
    <link href="<?php echo base_url(); ?>template_file/img/favicon/apple-icon-57x57.png" rel="apple-touch-icon"
          type="image/png">
    <link href="<?php echo base_url(); ?>template_file/img/favicon/favicon-16x16.png" rel="icon" type="image/png">
    <link href="<?php echo base_url(); ?>template_file/img/favicon/favicon.ico" rel="shortcut icon">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>template_file/AdminLTE/bootstrap/css/bootstrap.min.css"/>
    <!-- Font Awesome -->
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>node_modules/font-awesome/css/font-awesome.min.css"/>
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>template_file/AdminLTE/dist/css/animate.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>template_file/AdminLTE/dist/css/AdminLTE.min.css"/>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>template_file/AdminLTE/dist/css/skins/_all-skins.min.css"/>

    <link rel="stylesheet"
          href="<?php echo base_url(); ?>template_file/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"/>

    <link rel="stylesheet"
          href="<?php echo base_url(); ?>template_file/AdminLTE/plugins/bootstrap-formhelpers/bootstrap-formhelpers.min.css"/>

    <link rel="stylesheet" href="<?php echo base_url(); ?>template_file/AdminLTE/plugins/select2/select2.min.css"/>

    <link
        href="<?php echo base_url(); ?>node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css"
        rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url(); ?>template_file/css/admin/style.css"/>

    <?php if (isset($customStyles) && is_array($customStyles)) {
        foreach ($customStyles as $style) { ?>
            <link rel="stylesheet" href"<?php echo $style; ?>" />
        <?php }
    } ?>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>template_file/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script>
        var base_url = '<?php echo base_url(); ?>';
        var current_url = '<?php echo  current_url(); ?>';
    </script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>node_modules/noty/lib/noty.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>node_modules/noty/lib/themes/relax.css"/>
</head>
<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) == 'login'): ?>
<body class="hold-transition login-page">
<div class="login-box">
    <?php else: ?>
    <body class="hold-transition skin-blue sidebar-mini fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
        <?php $this->view('admin/header_top'); ?>
        <?php $this->view('admin/left_menu'); ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <?php endif; ?>
