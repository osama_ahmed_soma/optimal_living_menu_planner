<div class="login-logo">
    <img src="<?php echo base_url(); ?>template_file/img/logo.png" class="image-responsive" width="80%"/>
</div>

<?php echo validation_errors(); ?>


<?php if ($this->session->flashdata('captcha_error')) { ?>
    <div class="alert alert-danger alert-dismissible">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <?php echo $this->session->flashdata('captcha_error'); ?>
    </div>
<?php } ?>

<?php if (isset($loginError) && $loginError != ""): ?>
    <div class="alert alert-danger alert-dismissible">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <?php echo $loginError; ?>
    </div>
<?php endif; ?>

<div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <?php echo form_open('admin/login'); ?>
    <div class="form-group has-feedback">
        <input type="text" class="form-control" name="user_login" placeholder="Username"
               value="<?php echo set_value('user_login'); ?>">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="user_password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <?php if ($GLOBALS['captcha_required']) { ?>
        <div class="form-group">
            <div class="g-recaptcha" data-sitekey="<?php echo CAPTCHA_SITE_KEY; ?>"></div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-xs-8">

        </div>
        <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
    </div>
    <?php echo form_close(); ?>
    <a href="<?php echo base_url(); ?>admin/forgot_password">I forgot my password</a>
</div>