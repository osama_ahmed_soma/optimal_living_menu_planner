<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; <?php echo date('Y'); ?> <a target="new" href="http://reavertech.com/">Reaver Technologies</a>.</strong> All rights reserved.
</footer>