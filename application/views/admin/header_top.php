<header class="main-header">
    <a href="<?php echo base_url() ?>admin/" class="logo">
        <span class="logo-mini"><b>OL</b>MP</span>
        <span class="logo-lg"><b>OptimalLiving</b>MenuPlanner</span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo default_image($this->session->userdata('admin_sess')['picture']); ?>"
                             class="user-image" alt="User Image">
                        <span class="hidden-xs">
                            <?php echo $this->session->userdata('admin_sess')['firstname'] . ' ' . $this->session->userdata('admin_sess')['lastname']; ?>
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo default_image($this->session->userdata('admin_sess')['picture']); ?>"
                                 class="img-circle" alt="User Image">
                            <p>
                                <?php echo $this->session->userdata('admin_sess')['firstname'] . ' ' . $this->session->userdata('admin_sess')['lastname']; ?>
                                <small>Member since <?php date('M Y'); ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo base_url(); ?>admin/profile" class="btn btn-primary btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo base_url(); ?>admin/logout" class="btn btn-primary btn-flat">Sign
                                    out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>