<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo default_image($this->session->userdata('admin_sess')['picture']); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata('admin_sess')['firstname'] . ' ' . $this->session->userdata('admin_sess')['lastname']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'diet' &&  in_array($this->uri->segment(3), array('', 'index', 'diets') ) ) { echo 'active '; } ?>treeview">
                <a href="#">
                    <i class="fa fa-user-md"></i> <span>Diet Setup</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($this->uri->segment(1) === 'admin' && $this->uri->segment(2) === 'diet' && ($this->uri->segment(3) == '' || $this->uri->segment(3) == 'diets') ) { echo 'active'; } ?>"><a href="<?php echo base_url() ?>admin/diet"><i class="fa fa-users"></i> Diet List</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>