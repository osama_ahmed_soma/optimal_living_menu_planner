<section class="content-header">
    <h1>
        Diets List <a href="#" class="btn btn-primary create-diet-btn" data-toggle="modal" data-target="#diet_modal">Create
            Diet</a>
    </h1>
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Diet</li>
        <li class="active"> Diets List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12" id="ajax-container">
            <div class="box">
                <div class="box-body">
                    <table id="doctors_all" class="table table-bordered table-hover tablesorter customTableSort">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Sort Order</th>
                            <th>Created At</th>
                            <th>Action(s)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($diets)): ?>
                            <?php foreach ($diets as $diet): ?>
                                <tr id="diet_<?php echo $diet->id; ?>">
                                    <td><?php echo $diet->id; ?></td>
                                    <td class="name"><?php echo $diet->name; ?></td>
                                    <td class="description"><?php echo $diet->description; ?></td>
                                    <td class="sort_order"><?php echo $diet->sort_order; ?></td>
                                    <td class="created_at"><?php echo $diet->created_at; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-primary edit_diet" title="Edit" href="#"
                                               data-name="<?php echo $diet->name; ?>"
                                               data-description="<?php echo $diet->description; ?>"
                                               data-sort_order="<?php echo $diet->sort_order; ?>"
                                               data-id="<?php echo $diet->id; ?>"
                                               data-toggle="modal" data-target="#diet_modal">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="delete btn btn-sm btn-danger diet_remove"
                                               data-diet_id="<?php echo $diet->id; ?>" title="Delete">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="4">There is no Diet.</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Sort Order</th>
                            <th>Created At</th>
                            <th>Action(s)</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <?php echo (isset($pagination_link)) ? $pagination_link : ''; ?>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="diet_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('', array('class' => 'diet_form')); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create Diet</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" id="name"
                                   placeholder="Type New Diet Name" required/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" name="description" id="description"
                                   placeholder="Diet Description" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="sort_order">Order</label>
                            <input type="text" class="form-control" name="sort_order" id="sort_order"
                                   placeholder="0"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="id" value="0"/>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary action-btn">Create</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>


    function renderHTML() {
         $('#ajax-container').load(current_url+' .box', function () {
             // $(".customTableSort").trigger("update");
             $(".customTableSort").tablesorter();
         });
    }
    $(document).ready(function () {

        $('.create-diet-btn').on('click', function() {
            $("#diet_modal").on('show.bs.modal', function () {
                $('.diet_form input[name="name"], .diet_form input[name="description"], .diet_form input[name="sort_order"]').val('');
                $('.diet_form input[name="diet_id"]').val(0);
                $('.diet_form #myModalLabel').text("Create Diet");
                $('.diet_form .action-btn').text('Create');
            });
        });

        $(document).on('click', '.edit_diet', function (e) {
            $("#diet_modal").on('show.bs.modal', function () {
                $('.diet_form #myModalLabel').text("Edit Diet");
                $('.diet_form .action-btn').text('Update');
            });
        });

        $('.diet_form').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: base_url+'admin/diet/diet_ajax',
                data: $(this).serialize(),
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                console.log(data);
                var result = JSON.parse(data);

                if (result.bool) {

                    $('.diet_form input[name="name"], .diet_form input[name="description"], .diet_form input[name="sort_order"]').val('');
                    $('.diet_form input[name="diet_id"]').val(0);

                    $('#diet_modal').modal('hide');
                    notySuccess(result.message);
                    // show diet here
                    renderHTML(result.diet);
                } else {
                    if (typeof result.message == 'object') {
                        for (var key in result.message) {
                            if (result.message.hasOwnProperty(key)) {
                                notyError(result.message[key]);
                            }
                        }
                    }
                    if (typeof result.message == 'string') {
                        notyError(result.message);
                    }
                }
            });
            return false;
        });


        $(document).on('click', '.edit_diet', function (e) {
            attrs = $(this).data();
            $.each(attrs, function (k, v) {
                console.log($('.diet_form #' + k));
                if ( $('.diet_form #' + k).length ) {
                    $('.diet_form #' + k).val(v);
                }
            });

            $("#diet_modal").on('show.bs.modal', function () {
                $('.diet_form #myModalLabel').text("Edit Diet");
                $('.diet_form .action-btn').text('Update');
            });
        });


        function removeDiet(id, self) {
            $.ajax({
                url: base_url+'admin/diet/diet_ajax',
                data: {
                    remove: 'true',
                    diet_id: id
                },
                method: "POST"
            }).error(function () {

            }).done(function () {

            }).success(function (data) {
                //self.parent().parent().parent().remove();
                notySuccess('You Successfully delete diet.');
                renderHTML();
            });
        }

        $(document).on('click', '.diet_remove', function (e) {
            e.preventDefault();
            var self = $(this);
            var diet_id = $(this).data('diet_id');
            notyMessage('Are you sure that you want to DELETE', 'warning', {
                buttons: [
                    Noty.button('YES', 'btn btn-success', function ($noty) {
                        $noty.close();
                        notySuccess('Please wait......');
                        removeDiet(diet_id, self);
                    }, {id: 'button1', 'data-status': 'ok'}),

                    Noty.button('NO', 'btn btn-error', function ($noty) {
                        console.log('button 2 clicked');
                        $noty.close();
                    })
                ]
            });
            return false;
        });
    });
</script>