<?php

function verify_captcha() {
	$CI = & get_instance();
	$fields = array(
		'secret' => CAPTCHA_SECRET,
		'response' => trim($CI->input->post('g-recaptcha-response', true)),
		'remoteip' => get_ip_address()
	);
	$ch = curl_init("https://www.google.com/recaptcha/api/siteverify");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // will enable this later
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_CAINFO, APPPATH . '/helpers/www.google.com.cert');
	curl_setopt($ch, CURLOPT_TIMEOUT, 15);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
	$check = curl_exec($ch);
	if ($check) {
		$response = json_decode($check);
		curl_close($ch);
		return $response->success;
	}
	log_message('error', curl_error($ch));
	return false;
}

function captcha_required($required_failed_attempts_functionality = true) {
	$CI = & get_instance();
	if ($required_failed_attempts_functionality) {
		$GLOBALS['captcha_required'] = false;
		if (!$CI->session->userdata('failed_attempts')) {
			$CI->session->set_userdata('failed_attempts', 0);
		}
		if ($CI->session->userdata('failed_attempts') >= MAX_FAILED_ATTEMPTS) {
			$GLOBALS['captcha_required'] = true;
		}
		if ($GLOBALS['captcha_required'] == true && $CI->input->method() == 'post' && $CI->session->userdata('failed_attempts') > MAX_FAILED_ATTEMPTS) {
			if (!verify_captcha()) {
				$CI->session->set_flashdata('captcha_error', "Human verification failed <br>");
			}
		}
	} else {
		$GLOBALS['captcha_required'] = true;
		if ($CI->input->method() == 'post') {
			if (!verify_captcha()) {
				$GLOBALS['captcha_error'] = true; // used this one for validating captcha through AJAX
				$CI->session->set_flashdata('captcha_error', "Human verification failed <br>");
			}
		}
	}
}

function failed_attempt() {
	$CI = & get_instance();
	if ($CI->input->method() == 'post') {
		$CI->session->set_userdata('failed_attempts', $CI->session->userdata('failed_attempts') + 1);
	}
}

function reset_attempts() {
	$CI = & get_instance();
	$CI->session->set_userdata('failed_attempts', 0);
}
