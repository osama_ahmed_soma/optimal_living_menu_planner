jQuery(document).ready(function( $ ){

});
// notifications
function notySuccess(message)
{
    notyMessage(message, 'success');
}

function notyError(message) {
    notyMessage(message, 'error');
}

function notyConfirm(message, onSuccessCallback) {
    var params = {
        buttons: [
            {
                addClass: 'btn btn-primary',
                text: 'Ok',
                onClick: function($noty) {
                    $noty.close();
                    onSuccessCallback();
                }
            },
            {
                addClass: 'btn btn-danger',
                text: 'Cancel',
                onClick: function($noty) {
                    $noty.close();
                }
            }
        ]
    };

    notyMessage(message, 'warning', params);
}

function notyMessage(message, type, params) {
    var defaultParams = {
        text: message,
        theme: 'relax',
        type: type,
        layout: 'topRight',
        timeout: 20000 * 1000,
        killer: true,
        animation: {
            open: 'animated bounceInRight',
            close: 'animated bounceOutRight'
        }
    };

    new Noty($.extend(defaultParams, params)).show();
}