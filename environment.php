<?php

if (basename($_SERVER['SCRIPT_NAME']) == basename(__FILE__)) {
	header("HTTP/1.1 404 Not Found");
	die('Error 404 - Page Not Found');
}

// using this function because filter_input does not work well with PHP/FastCGI setup
// source: http://stackoverflow.com/a/36205923/925659
function filter_input_fix($type, $variable_name, $filter = FILTER_DEFAULT, $options = NULL)
{
	$checkTypes = [
		INPUT_GET,
		INPUT_POST,
		INPUT_COOKIE
	];

	if ($options === NULL) {
		// No idea if this should be here or not
		// Maybe someone could let me know if this should be removed?
		$options = FILTER_NULL_ON_FAILURE;
	}

	if (in_array($type, $checkTypes) || filter_has_var($type, $variable_name)) {
		return filter_input($type, $variable_name, $filter, $options);
	} else if ($type == INPUT_SERVER && isset($_SERVER[$variable_name])) {
		return filter_var($_SERVER[$variable_name], $filter, $options);
	} else if ($type == INPUT_ENV && isset($_ENV[$variable_name])) {
		return filter_var($_ENV[$variable_name], $filter, $options);
	} else {
		return NULL;
	}
}

if (php_sapi_name() === 'cli' OR defined('STDIN'))
{
	$_SERVER['SCRIPT_NAME'] = 'index.php';
	$_SERVER['SERVER_NAME'] = 'localhost';
	$_SERVER['SERVER_PORT'] = 80;
	$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
	$_SERVER['HTTP_HOST'] = 'localhost';
}

function get_ip_address()
{
	$ip_keys = array('HTTP_CF_CONNECTING_IP', 'HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
	foreach ($ip_keys as $key) {
		if (array_key_exists($key, $_SERVER) === true) {
			foreach (explode(',', $_SERVER[$key]) as $ip) {
				// trim for safety measures
				$ip = trim($ip);
				// attempt to validate IP
				if (validate_ip($ip)) {
					return $ip;
				}
			}
		}
	}

	return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
}

/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
function validate_ip($ip)
{
	if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
		return false;
	}
	return true;
}

function get_server_ip_address()
{
	return (filter_input_fix(INPUT_SERVER, 'SERVER_ADDR', FILTER_VALIDATE_IP)) ? filter_input_fix(INPUT_SERVER, 'SERVER_ADDR', FILTER_VALIDATE_IP) : gethostbyname(getHostName());
}

/*
 * ---------------------------------------------------------------
 * MY DEBUG CONFIG
 * ---------------------------------------------------------------
 */

$client_ip = get_ip_address();
$server_ip = get_server_ip_address();

$debug_lan_ips = array('127.0.0', '192.168.0', '192.168.1', '192.168.8');
$server_local_ips = array('127.0.0', '127.0.1');
$debug_ips = array(
	gethostbyname('laptop.redirectme.net'),
	gethostbyname('reavertech.ddns.net'),
	'103.214.201.3',
	gethostbyname('salmanr.hopto.org'),
);

// CAUTION: use it only when you have HTTPS connection available
$enable_debuging_with_http_header = false;
$debug_http_header_exists = false;
if ($enable_debuging_with_http_header) {
	$debug_http_header_exists = in_array(filter_input_fix(INPUT_SERVER, 'HTTP_DEBUG', FILTER_SANITIZE_FULL_SPECIAL_CHARS), array('^p13@s3*d3bu9'));
	if (filter_input_fix(INPUT_SERVER, 'SERVER_PORT') != 443) {
		die('Cannot use HTTP debugging because HTTPS connection is not available');
	}
}

define('DEBUG', (in_array(substr($client_ip, 0, strrpos($client_ip, '.')), $debug_lan_ips) || ($debug_http_header_exists === TRUE) || in_array($client_ip, $debug_ips)));

define('LOCAL', in_array(substr($server_ip, 0, strrpos($server_ip, '.')), $server_local_ips));

if (DEBUG === false) {
	$_SERVER['CI_ENV'] = 'production';
}

if (DEBUG) {
	error_reporting(E_ALL ^ E_NOTICE);
	ini_set('html_errors', 1);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	ini_set('log_errors', 1);
	ini_set('track_errors', 1);
}

//echo '<pre>';
//echo PHP_EOL;
//print_r($client_ip);
//echo PHP_EOL;
//print_r($debug_ips);
//echo PHP_EOL;
//print_r($debug_lan_ips);
//echo PHP_EOL;
//print_r($server_ip);
//echo PHP_EOL;
//var_dump(DEBUG);
//echo PHP_EOL;
//var_dump(LOCAL);
//echo PHP_EOL;
//print_r(get_defined_vars());
//echo '</pre>';
//die;
