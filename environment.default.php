<?php

if (basename($_SERVER['SCRIPT_NAME']) == basename(__FILE__)) {
	header("HTTP/1.1 404 Not Found");
	die('Error 404 - Page Not Found');
}

// using this function because filter_input() doesn't work with INPUT_SERVER or INPUT_ENV when you use FASTCGI
// source: http://stackoverflow.com/a/36205923/925659
function filter_input_fix($type, $variable_name, $filter = FILTER_DEFAULT, $options = NULL) {
	$checkTypes = [
		INPUT_GET,
		INPUT_POST,
		INPUT_COOKIE
	];

	if ($options === NULL) {
		// No idea if this should be here or not
		// Maybe someone could let me know if this should be removed?
		$options = FILTER_NULL_ON_FAILURE;
	}

	if (in_array($type, $checkTypes) || filter_has_var($type, $variable_name)) {
		return filter_input($type, $variable_name, $filter, $options);
	} else if ($type == INPUT_SERVER && isset($_SERVER[$variable_name])) {
		return filter_var($_SERVER[$variable_name], $filter, $options);
	} else if ($type == INPUT_ENV && isset($_ENV[$variable_name])) {
		return filter_var($_ENV[$variable_name], $filter, $options);
	} else {
		return NULL;
	}
}

if (php_sapi_name() === 'cli' OR defined('STDIN')) {
	$_SERVER['SCRIPT_NAME'] = 'index.php';
	$_SERVER['SERVER_NAME'] = 'localhost';
	$_SERVER['SERVER_PORT'] = 80;
	$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
	$_SERVER['HTTP_HOST'] = 'localhost';
}

function get_ip_address() {
	$ip_keys = array('HTTP_CF_CONNECTING_IP', 'HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
	foreach ($ip_keys as $key) {
		if (array_key_exists($key, $_SERVER) === true) {
			foreach (explode(',', $_SERVER[$key]) as $ip) {
				// trim for safety measures
				$ip = trim($ip);
				// attempt to validate IP
				if (validate_ip($ip)) {
					return $ip;
				}
			}
		}
	}

	return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
}

/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
function validate_ip($ip) {
	if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
		return false;
	}
	return true;
}

function get_server_ip_address() {
	return (filter_input_fix(INPUT_SERVER, 'SERVER_ADDR', FILTER_VALIDATE_IP)) ? filter_input_fix(INPUT_SERVER, 'SERVER_ADDR', FILTER_VALIDATE_IP) : gethostbyname(getHostName());
}

/*
 * ---------------------------------------------------------------
 * MY DEBUG CONFIG
 * ---------------------------------------------------------------
 */

$client_ip = get_ip_address();
$server_ip = get_server_ip_address();

$debug_lan_ips = array('127.0.0', '192.168.0', '192.168.1', '192.168.8');
$server_local_ips = array('127.0.0', '127.0.1');

define('LOCAL', in_array(substr($server_ip, 0, strrpos($server_ip, '.')), $server_local_ips));

$debug_ips = [];

if (!LOCAL) {
	$debug_ips = array(
		gethostbyname('laptop.redirectme.net'),
		gethostbyname('reavertech.ddns.net'),
		'103.214.201.3',
		gethostbyname('salmanr.hopto.org'),
	);
}

// CAUTION: use it only when you have HTTPS connection available
$enable_debuging_with_http_header = false;
$debug_http_header_exists = false;
if ($enable_debuging_with_http_header) {
	$debug_http_header_exists = in_array(filter_input_fix(INPUT_SERVER, 'HTTP_DEBUG', FILTER_SANITIZE_FULL_SPECIAL_CHARS), array('^p13@s3*d3bu9'));
	if (filter_input_fix(INPUT_SERVER, 'SERVER_PORT') != 443) {
		die('Cannot use HTTP debugging because HTTPS connection is not available');
	}
}

define('DEBUG', (in_array(substr($client_ip, 0, strrpos($client_ip, '.')), $debug_lan_ips) || ($debug_http_header_exists === TRUE) || in_array($client_ip, $debug_ips)));

if (DEBUG === false) {
	$_SERVER['CI_ENV'] = 'production';
}

ini_set('display_errors', 0);
ini_set('html_errors', 0);
ini_set('log_errors', 1);
//ini_set('error_log', 'php_errors.log');
error_reporting(0);

if (DEBUG) {
	error_reporting(E_ALL ^ E_NOTICE);
	ini_set('html_errors', 1);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	ini_set('log_errors', 1);
	ini_set('track_errors', 1);
}

if (DEBUG) {
	header('X-DEBUG: 1');
}

$websitename = "Menu Planner";
$page = explode('/', filter_input_fix(INPUT_SERVER, 'PHP_SELF'));
$count = count($page);
$pagephpfilename = $page[1];
$pageActual = $page = $page[$count - 1];
$__page = pathinfo($page, PATHINFO_FILENAME);

// need to work more on this, REQUEST_URI is not so good
//$dynamic_url = rtrim(str_replace($page, '', filter_input_fix(INPUT_SERVER, 'REQUEST_URI')), '/');

$PHP_SELF = filter_input_fix(INPUT_SERVER, 'PHP_SELF');
$PHP_SELF_pathinfo = pathinfo($PHP_SELF);

$dynamic_url = rtrim($PHP_SELF_pathinfo['dirname'], '/');

//echo '<pre>';
//var_dump($_SERVER);
//var_dump($dynamic_url);
//var_dump(filter_input_fix(INPUT_SERVER, 'DOCUMENT_ROOT') . $dynamic_url);
//var_dump(is_dir(filter_input_fix(INPUT_SERVER, 'DOCUMENT_ROOT') . $dynamic_url));
//if (!is_dir(filter_input_fix(INPUT_SERVER, 'DOCUMENT_ROOT') . $dynamic_url)) {
//	$dynamic_url = '';
//}
//var_dump($dynamic_url);
//var_dump(dirname(__FILE__));
//var_dump(basename(__DIR__));
//echo '</pre>';

if (IS_CLI) {
	$dynamic_path = dirname(dirname(__DIR__)) . '/';
} else {
	$dynamic_path = dirname(getcwd());
}

//var_dump($dynamic_path);
//die;

//define('SERVER_ROOT', filter_input_fix(INPUT_SERVER, 'DOCUMENT_ROOT') . $dynamic_path);
define('SERVER_ROOT', $dynamic_path);

set_include_path(get_include_path() . PATH_SEPARATOR . SERVER_ROOT . PATH_SEPARATOR . SERVER_ROOT . '/core/includes');

$REQUEST_SCHEME = 'http';
$HTTPS = 0;

if (filter_input_fix(INPUT_SERVER, 'SERVER_PORT') == '443') {
	$REQUEST_SCHEME = 'https';
	$HTTPS = 1;
}

define('HTTPS', $HTTPS);

define('DOMAIN', filter_input_fix(INPUT_SERVER, 'HTTP_HOST'));
define('BASE_URL', $REQUEST_SCHEME . '://' . DOMAIN . $dynamic_url);

//echo '<pre>';
//echo PHP_EOL;
//var_dump(get_include_path());
//echo PHP_EOL;
//var_dump(SERVER_ROOT);
//echo PHP_EOL;
//var_dump(BASE_URL);
//echo PHP_EOL;
//var_dump($client_ip);
//echo PHP_EOL;
//var_dump($debug_ips);
//echo PHP_EOL;
//var_dump($debug_lan_ips);
//echo PHP_EOL;
//var_dump($server_ip);
//echo PHP_EOL;
//var_dump(DEBUG);
//echo PHP_EOL;
//var_dump(LOCAL);
//echo PHP_EOL;
//var_dump(get_defined_vars());
//echo '</pre>';
//die;
// things are set and working, now unset variables because I love to see empty RAM
unset($REQUEST_SCHEME);
unset($HTTPS_AVAILABLE);
